﻿namespace Toolbar
{
    /// <summary>
    /// Toolbar視窗
    /// </summary>
    partial class MainForm
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel_main = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel_top = new System.Windows.Forms.TableLayoutPanel();
            this.button_top_tab = new System.Windows.Forms.Button();
            this.tableLayoutPanel_top_function = new System.Windows.Forms.TableLayoutPanel();
            this.button_exit = new System.Windows.Forms.Button();
            this.button_func3 = new System.Windows.Forms.Button();
            this.button_func2 = new System.Windows.Forms.Button();
            this.button_func1 = new System.Windows.Forms.Button();
            this.tableLayoutPanel_right = new System.Windows.Forms.TableLayoutPanel();
            this.button_right_tab = new System.Windows.Forms.Button();
            this.tableLayoutPanel_right_function = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel_left = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel_left_function = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel_bottom = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel_bottom_function = new System.Windows.Forms.TableLayoutPanel();
            this.button_left_tab = new System.Windows.Forms.Button();
            this.button_bottom_tab = new System.Windows.Forms.Button();
            this.tableLayoutPanel_main.SuspendLayout();
            this.tableLayoutPanel_top.SuspendLayout();
            this.tableLayoutPanel_top_function.SuspendLayout();
            this.tableLayoutPanel_right.SuspendLayout();
            this.tableLayoutPanel_left.SuspendLayout();
            this.tableLayoutPanel_bottom.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel_main
            // 
            this.tableLayoutPanel_main.ColumnCount = 3;
            this.tableLayoutPanel_main.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.tableLayoutPanel_main.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 265F));
            this.tableLayoutPanel_main.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.tableLayoutPanel_main.Controls.Add(this.tableLayoutPanel_top, 1, 0);
            this.tableLayoutPanel_main.Controls.Add(this.tableLayoutPanel_right, 2, 1);
            this.tableLayoutPanel_main.Controls.Add(this.tableLayoutPanel_left, 0, 1);
            this.tableLayoutPanel_main.Controls.Add(this.tableLayoutPanel_bottom, 1, 2);
            this.tableLayoutPanel_main.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel_main.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel_main.Name = "tableLayoutPanel_main";
            this.tableLayoutPanel_main.RowCount = 3;
            this.tableLayoutPanel_main.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.tableLayoutPanel_main.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 265F));
            this.tableLayoutPanel_main.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.tableLayoutPanel_main.Size = new System.Drawing.Size(445, 445);
            this.tableLayoutPanel_main.TabIndex = 0;
            // 
            // tableLayoutPanel_top
            // 
            this.tableLayoutPanel_top.ColumnCount = 3;
            this.tableLayoutPanel_top.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 215F));
            this.tableLayoutPanel_top.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel_top.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel_top.Controls.Add(this.button_top_tab, 1, 1);
            this.tableLayoutPanel_top.Controls.Add(this.tableLayoutPanel_top_function, 0, 0);
            this.tableLayoutPanel_top.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel_top.Location = new System.Drawing.Point(90, 0);
            this.tableLayoutPanel_top.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel_top.Name = "tableLayoutPanel_top";
            this.tableLayoutPanel_top.RowCount = 2;
            this.tableLayoutPanel_top.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel_top.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel_top.Size = new System.Drawing.Size(265, 90);
            this.tableLayoutPanel_top.TabIndex = 0;
            // 
            // button_top_tab
            // 
            this.button_top_tab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_top_tab.FlatAppearance.BorderSize = 0;
            this.button_top_tab.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_top_tab.Location = new System.Drawing.Point(215, 70);
            this.button_top_tab.Margin = new System.Windows.Forms.Padding(0);
            this.button_top_tab.Name = "button_top_tab";
            this.button_top_tab.Size = new System.Drawing.Size(30, 20);
            this.button_top_tab.TabIndex = 0;
            this.button_top_tab.Text = "tab";
            this.button_top_tab.UseVisualStyleBackColor = true;
            this.button_top_tab.Click += new System.EventHandler(this.button_tab_Click);
            this.button_top_tab.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button_tab_MouseDown);
            this.button_top_tab.MouseMove += new System.Windows.Forms.MouseEventHandler(this.button_tab_MouseMove);
            this.button_top_tab.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button_tab_MouseUp);
            // 
            // tableLayoutPanel_top_function
            // 
            this.tableLayoutPanel_top_function.ColumnCount = 7;
            this.tableLayoutPanel_top.SetColumnSpan(this.tableLayoutPanel_top_function, 3);
            this.tableLayoutPanel_top_function.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel_top_function.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutPanel_top_function.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel_top_function.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutPanel_top_function.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel_top_function.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutPanel_top_function.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel_top_function.Controls.Add(this.button_exit, 6, 0);
            this.tableLayoutPanel_top_function.Controls.Add(this.button_func3, 4, 0);
            this.tableLayoutPanel_top_function.Controls.Add(this.button_func2, 2, 0);
            this.tableLayoutPanel_top_function.Controls.Add(this.button_func1, 0, 0);
            this.tableLayoutPanel_top_function.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel_top_function.Location = new System.Drawing.Point(5, 5);
            this.tableLayoutPanel_top_function.Margin = new System.Windows.Forms.Padding(5);
            this.tableLayoutPanel_top_function.Name = "tableLayoutPanel_top_function";
            this.tableLayoutPanel_top_function.RowCount = 1;
            this.tableLayoutPanel_top_function.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel_top_function.Size = new System.Drawing.Size(255, 60);
            this.tableLayoutPanel_top_function.TabIndex = 1;
            // 
            // button_exit
            // 
            this.button_exit.Location = new System.Drawing.Point(195, 0);
            this.button_exit.Margin = new System.Windows.Forms.Padding(0);
            this.button_exit.Name = "button_exit";
            this.button_exit.Size = new System.Drawing.Size(60, 60);
            this.button_exit.TabIndex = 2;
            this.button_exit.Text = "Exit";
            this.button_exit.UseVisualStyleBackColor = true;
            this.button_exit.Click += new System.EventHandler(this.button_exit_Click);
            // 
            // button_func3
            // 
            this.button_func3.Location = new System.Drawing.Point(130, 0);
            this.button_func3.Margin = new System.Windows.Forms.Padding(0);
            this.button_func3.Name = "button_func3";
            this.button_func3.Size = new System.Drawing.Size(60, 60);
            this.button_func3.TabIndex = 2;
            this.button_func3.Text = "func3";
            this.button_func3.UseVisualStyleBackColor = true;
            // 
            // button_func2
            // 
            this.button_func2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_func2.Location = new System.Drawing.Point(65, 0);
            this.button_func2.Margin = new System.Windows.Forms.Padding(0);
            this.button_func2.Name = "button_func2";
            this.button_func2.Size = new System.Drawing.Size(60, 60);
            this.button_func2.TabIndex = 1;
            this.button_func2.Text = "func2";
            this.button_func2.UseVisualStyleBackColor = true;
            // 
            // button_func1
            // 
            this.button_func1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_func1.Location = new System.Drawing.Point(0, 0);
            this.button_func1.Margin = new System.Windows.Forms.Padding(0);
            this.button_func1.Name = "button_func1";
            this.button_func1.Size = new System.Drawing.Size(60, 60);
            this.button_func1.TabIndex = 0;
            this.button_func1.Text = "func1";
            this.button_func1.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel_right
            // 
            this.tableLayoutPanel_right.ColumnCount = 2;
            this.tableLayoutPanel_right.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel_right.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel_right.Controls.Add(this.button_right_tab, 0, 1);
            this.tableLayoutPanel_right.Controls.Add(this.tableLayoutPanel_right_function, 1, 0);
            this.tableLayoutPanel_right.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel_right.Location = new System.Drawing.Point(355, 90);
            this.tableLayoutPanel_right.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel_right.Name = "tableLayoutPanel_right";
            this.tableLayoutPanel_right.RowCount = 3;
            this.tableLayoutPanel_right.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel_right.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel_right.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 215F));
            this.tableLayoutPanel_right.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel_right.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel_right.Size = new System.Drawing.Size(90, 265);
            this.tableLayoutPanel_right.TabIndex = 1;
            // 
            // button_right_tab
            // 
            this.button_right_tab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_right_tab.FlatAppearance.BorderSize = 0;
            this.button_right_tab.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_right_tab.Location = new System.Drawing.Point(0, 20);
            this.button_right_tab.Margin = new System.Windows.Forms.Padding(0);
            this.button_right_tab.Name = "button_right_tab";
            this.button_right_tab.Size = new System.Drawing.Size(20, 30);
            this.button_right_tab.TabIndex = 4;
            this.button_right_tab.Text = "tab";
            this.button_right_tab.UseVisualStyleBackColor = true;
            this.button_right_tab.Click += new System.EventHandler(this.button_tab_Click);
            this.button_right_tab.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button_tab_MouseDown);
            this.button_right_tab.MouseMove += new System.Windows.Forms.MouseEventHandler(this.button_tab_MouseMove);
            this.button_right_tab.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button_tab_MouseUp);
            // 
            // tableLayoutPanel_right_function
            // 
            this.tableLayoutPanel_right_function.ColumnCount = 1;
            this.tableLayoutPanel_right_function.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel_right_function.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel_right_function.Location = new System.Drawing.Point(25, 5);
            this.tableLayoutPanel_right_function.Margin = new System.Windows.Forms.Padding(5);
            this.tableLayoutPanel_right_function.Name = "tableLayoutPanel_right_function";
            this.tableLayoutPanel_right_function.RowCount = 7;
            this.tableLayoutPanel_right.SetRowSpan(this.tableLayoutPanel_right_function, 3);
            this.tableLayoutPanel_right_function.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel_right_function.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutPanel_right_function.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel_right_function.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutPanel_right_function.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel_right_function.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutPanel_right_function.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel_right_function.Size = new System.Drawing.Size(60, 255);
            this.tableLayoutPanel_right_function.TabIndex = 0;
            // 
            // tableLayoutPanel_left
            // 
            this.tableLayoutPanel_left.ColumnCount = 2;
            this.tableLayoutPanel_left.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel_left.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel_left.Controls.Add(this.button_left_tab, 1, 1);
            this.tableLayoutPanel_left.Controls.Add(this.tableLayoutPanel_left_function, 0, 0);
            this.tableLayoutPanel_left.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel_left.Location = new System.Drawing.Point(0, 90);
            this.tableLayoutPanel_left.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel_left.Name = "tableLayoutPanel_left";
            this.tableLayoutPanel_left.RowCount = 3;
            this.tableLayoutPanel_left.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel_left.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel_left.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 215F));
            this.tableLayoutPanel_left.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel_left.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel_left.Size = new System.Drawing.Size(90, 265);
            this.tableLayoutPanel_left.TabIndex = 2;
            // 
            // tableLayoutPanel_left_function
            // 
            this.tableLayoutPanel_left_function.ColumnCount = 1;
            this.tableLayoutPanel_left_function.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel_left_function.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel_left_function.Location = new System.Drawing.Point(5, 5);
            this.tableLayoutPanel_left_function.Margin = new System.Windows.Forms.Padding(5);
            this.tableLayoutPanel_left_function.Name = "tableLayoutPanel_left_function";
            this.tableLayoutPanel_left_function.RowCount = 7;
            this.tableLayoutPanel_left.SetRowSpan(this.tableLayoutPanel_left_function, 3);
            this.tableLayoutPanel_left_function.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel_left_function.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutPanel_left_function.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel_left_function.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutPanel_left_function.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel_left_function.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutPanel_left_function.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel_left_function.Size = new System.Drawing.Size(60, 255);
            this.tableLayoutPanel_left_function.TabIndex = 0;
            // 
            // tableLayoutPanel_bottom
            // 
            this.tableLayoutPanel_bottom.ColumnCount = 3;
            this.tableLayoutPanel_bottom.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 215F));
            this.tableLayoutPanel_bottom.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel_bottom.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel_bottom.Controls.Add(this.button_bottom_tab, 1, 0);
            this.tableLayoutPanel_bottom.Controls.Add(this.tableLayoutPanel_bottom_function, 0, 1);
            this.tableLayoutPanel_bottom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel_bottom.Location = new System.Drawing.Point(90, 355);
            this.tableLayoutPanel_bottom.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel_bottom.Name = "tableLayoutPanel_bottom";
            this.tableLayoutPanel_bottom.RowCount = 2;
            this.tableLayoutPanel_bottom.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel_bottom.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel_bottom.Size = new System.Drawing.Size(265, 90);
            this.tableLayoutPanel_bottom.TabIndex = 3;
            // 
            // tableLayoutPanel_bottom_function
            // 
            this.tableLayoutPanel_bottom_function.ColumnCount = 7;
            this.tableLayoutPanel_bottom.SetColumnSpan(this.tableLayoutPanel_bottom_function, 3);
            this.tableLayoutPanel_bottom_function.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel_bottom_function.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutPanel_bottom_function.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel_bottom_function.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutPanel_bottom_function.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel_bottom_function.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutPanel_bottom_function.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel_bottom_function.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel_bottom_function.Location = new System.Drawing.Point(5, 25);
            this.tableLayoutPanel_bottom_function.Margin = new System.Windows.Forms.Padding(5);
            this.tableLayoutPanel_bottom_function.Name = "tableLayoutPanel_bottom_function";
            this.tableLayoutPanel_bottom_function.RowCount = 1;
            this.tableLayoutPanel_bottom_function.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel_bottom_function.Size = new System.Drawing.Size(255, 60);
            this.tableLayoutPanel_bottom_function.TabIndex = 1;
            // 
            // button_left_tab
            // 
            this.button_left_tab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_left_tab.FlatAppearance.BorderSize = 0;
            this.button_left_tab.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_left_tab.Location = new System.Drawing.Point(70, 20);
            this.button_left_tab.Margin = new System.Windows.Forms.Padding(0);
            this.button_left_tab.Name = "button_left_tab";
            this.button_left_tab.Size = new System.Drawing.Size(20, 30);
            this.button_left_tab.TabIndex = 4;
            this.button_left_tab.Text = "tab";
            this.button_left_tab.UseVisualStyleBackColor = true;
            this.button_left_tab.Click += new System.EventHandler(this.button_tab_Click);
            this.button_left_tab.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button_tab_MouseDown);
            this.button_left_tab.MouseMove += new System.Windows.Forms.MouseEventHandler(this.button_tab_MouseMove);
            this.button_left_tab.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button_tab_MouseUp);
            // 
            // button_bottom_tab
            // 
            this.button_bottom_tab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_bottom_tab.FlatAppearance.BorderSize = 0;
            this.button_bottom_tab.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_bottom_tab.Location = new System.Drawing.Point(215, 0);
            this.button_bottom_tab.Margin = new System.Windows.Forms.Padding(0);
            this.button_bottom_tab.Name = "button_bottom_tab";
            this.button_bottom_tab.Size = new System.Drawing.Size(30, 20);
            this.button_bottom_tab.TabIndex = 5;
            this.button_bottom_tab.Text = "tab";
            this.button_bottom_tab.UseVisualStyleBackColor = true;
            this.button_bottom_tab.Click += new System.EventHandler(this.button_tab_Click);
            this.button_bottom_tab.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button_tab_MouseDown);
            this.button_bottom_tab.MouseMove += new System.Windows.Forms.MouseEventHandler(this.button_tab_MouseMove);
            this.button_bottom_tab.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button_tab_MouseUp);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(445, 445);
            this.ControlBox = false;
            this.Controls.Add(this.tableLayoutPanel_main);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Form1";
            this.TopMost = true;
            this.tableLayoutPanel_main.ResumeLayout(false);
            this.tableLayoutPanel_top.ResumeLayout(false);
            this.tableLayoutPanel_top_function.ResumeLayout(false);
            this.tableLayoutPanel_right.ResumeLayout(false);
            this.tableLayoutPanel_left.ResumeLayout(false);
            this.tableLayoutPanel_bottom.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel_main;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel_top;
        private System.Windows.Forms.Button button_top_tab;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel_top_function;
        private System.Windows.Forms.Button button_exit;
        private System.Windows.Forms.Button button_func3;
        private System.Windows.Forms.Button button_func2;
        private System.Windows.Forms.Button button_func1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel_right;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel_right_function;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel_left;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel_left_function;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel_bottom;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel_bottom_function;
        private System.Windows.Forms.Button button_right_tab;
        private System.Windows.Forms.Button button_left_tab;
        private System.Windows.Forms.Button button_bottom_tab;
    }
}

