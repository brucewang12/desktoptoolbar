﻿using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace Toolbar
{
    public partial class MainForm : Form
    {
        /// <summary>
        /// 建構式
        /// </summary>
        public MainForm()
        {
            InitializeComponent();
            this.Location = new Point { X = 0 - TOOLBAR_HEIGHT - TAB_HEIGHT, Y = 0 - TOOLBAR_HEIGHT };

            // 防止窗體閃屏
            SetStyle(
                ControlStyles.UserPaint |
                ControlStyles.AllPaintingInWmPaint |
                ControlStyles.OptimizedDoubleBuffer |
                ControlStyles.ResizeRedraw |
                ControlStyles.SupportsTransparentBackColor, true);
            UpdateStyles();
            ChangeRegion(_curr_direction, _is_hide);
        }

        // 改變視窗遮罩座標
        private void ChangeRegion(Direction direction, bool is_hide)
        {
            using (GraphicsPath path = new GraphicsPath())
            {
                if (is_hide)
                {
                    path.AddLines(_hide_region_list[(int)direction]);
                }
                else
                {
                    path.AddLines(_region_list[(int)direction]);
                }
                this.Region = new Region(path);
            }
        }

        // 改變視窗位置
        private void ChangeLocation(Direction direction, bool is_hide)
        {
            Point new_location = new Point();
            switch (direction)
            {
                case Direction.Top:
                    {
                        new_location.X = Cursor.Position.X - (FORM_WIDTH - TOOLBAR_HEIGHT - TAB_HEIGHT - TAB_DISTANCE - TAB_WIDTH / 2);
                        new_location.Y = (is_hide ? -TOOLBAR_HEIGHT : 0);
                        break;
                    }
                case Direction.Right:
                    {
                        new_location.X = _resolution.Width - FORM_WIDTH + (is_hide ? TOOLBAR_HEIGHT : 0);
                        new_location.Y = Cursor.Position.Y - TAB_WIDTH / 2 - TAB_DISTANCE - TOOLBAR_HEIGHT - TAB_HEIGHT;
                        break;
                    }
                case Direction.Bottom:
                    {
                        new_location.X = Cursor.Position.X - (FORM_WIDTH - TOOLBAR_HEIGHT - TAB_HEIGHT - TAB_DISTANCE - TAB_WIDTH / 2);
                        new_location.Y = _resolution.Height - FORM_HEIGHT + (is_hide ? TOOLBAR_HEIGHT : 0);
                        break;
                    }
                case Direction.Left:
                    {
                        new_location.X = (is_hide ? -TOOLBAR_HEIGHT : 0);
                        new_location.Y = Cursor.Position.Y - TAB_WIDTH / 2 - TAB_DISTANCE - TOOLBAR_HEIGHT - TAB_HEIGHT;
                        break;
                    }
            }
            this.Location = LimitControlMove(new_location);
        }

        // 切換Toolbar隱藏狀態
        private void SwitchHideState()
        {
            _is_hide = !_is_hide;
            switch (_curr_direction)
            {
                case Direction.Top:
                    {
                        if (_is_hide)
                        {
                            this.Location = new Point { X = this.Location.X, Y = - TOOLBAR_HEIGHT };
                        }
                        else
                        {
                            this.Location = new Point { X = this.Location.X, Y = 0 };
                        }
                        break;
                    }
                case Direction.Right:
                    {
                        if (_is_hide)
                        {
                            this.Location = new Point { X = _resolution.Width - (FORM_WIDTH - TOOLBAR_HEIGHT), Y = this.Location.Y };
                        }
                        else
                        {
                            this.Location = new Point { X = _resolution.Width - FORM_WIDTH, Y = this.Location.Y };
                        }
                        break;
                    }
                case Direction.Bottom:
                    {
                        if (_is_hide)
                        {
                            this.Location = new Point { X = this.Location.X, Y = _resolution.Height - FORM_HEIGHT + TOOLBAR_HEIGHT };
                        }
                        else
                        {
                            this.Location = new Point { X = this.Location.X, Y = _resolution.Height - FORM_HEIGHT };
                        }
                        break;
                    }
                case Direction.Left:
                    {
                        if (_is_hide)
                        {
                            this.Location = new Point { X = -TOOLBAR_HEIGHT, Y = this.Location.Y };
                        }
                        else
                        {
                            this.Location = new Point { X = 0, Y = this.Location.Y };
                        }
                        break;
                    }
            }
            ChangeRegion(_curr_direction, _is_hide);
        }

        // 將滑鼠移動後座標與移動前座標計算出滑鼠位移量
        private Point GetMouseMoveDisplacement()
        {
            return new Point(Cursor.Position.X - _last_mouse_position.X
                , Cursor.Position.Y - _last_mouse_position.Y);
        }

        // Toolbar拖曳移動位置限制
        private Point LimitControlMove(Point new_location)
        {
            switch (_curr_direction)
            {
                case Direction.Top:
                    {
                        new_location.Y = (_is_hide ? -TOOLBAR_HEIGHT : 0);
                        if (new_location.X < -TOOLBAR_HEIGHT - TAB_HEIGHT)
                        {
                            new_location.X = -TOOLBAR_HEIGHT - TAB_HEIGHT;
                        }
                        else if (new_location.X > _resolution.Size.Width - TOOLBAR_WIDTH - TOOLBAR_HEIGHT - TAB_HEIGHT)
                        {
                            new_location.X = _resolution.Size.Width - TOOLBAR_WIDTH - TOOLBAR_HEIGHT - TAB_HEIGHT;
                        }
                        else
                        {
                            _last_mouse_position = Cursor.Position;
                        }
                        break;
                    }
                case Direction.Right:
                    {
                        new_location.X = _resolution.Width - FORM_WIDTH + (_is_hide ? TOOLBAR_HEIGHT : 0);
                        if (new_location.Y < -TOOLBAR_HEIGHT - TAB_HEIGHT)
                        {
                            new_location.Y = -TOOLBAR_HEIGHT - TAB_HEIGHT;
                        }
                        else if (new_location.Y > _resolution.Height - TOOLBAR_WIDTH - TAB_HEIGHT - TOOLBAR_HEIGHT)
                        {
                            new_location.Y = _resolution.Height - TOOLBAR_WIDTH - TAB_HEIGHT - TOOLBAR_HEIGHT;
                        }
                        else
                        {
                            _last_mouse_position = Cursor.Position;
                        }
                        break;
                    }
                case Direction.Bottom:
                    {
                        new_location.Y = _resolution.Height -FORM_HEIGHT + (_is_hide ? TOOLBAR_HEIGHT : 0);
                        if (new_location.X < -TOOLBAR_HEIGHT - TAB_HEIGHT)
                        {
                            new_location.X = -TOOLBAR_HEIGHT - TAB_HEIGHT;
                        }
                        else if (new_location.X > _resolution.Size.Width - TOOLBAR_WIDTH - TOOLBAR_HEIGHT - TAB_HEIGHT)
                        {
                            new_location.X = _resolution.Size.Width - TOOLBAR_WIDTH - TOOLBAR_HEIGHT - TAB_HEIGHT;
                        }
                        else
                        {
                            _last_mouse_position = Cursor.Position;
                        }
                        break;
                    }
                case Direction.Left:
                    {
                        new_location.X = (_is_hide ? -TOOLBAR_HEIGHT : 0);
                        if (new_location.Y < -TOOLBAR_HEIGHT - TAB_HEIGHT)
                        {
                            new_location.Y = -TOOLBAR_HEIGHT - TAB_HEIGHT;
                        }
                        else if (new_location.Y > _resolution.Height - TOOLBAR_WIDTH - TAB_HEIGHT - TOOLBAR_HEIGHT)
                        {
                            new_location.Y = _resolution.Height - TOOLBAR_WIDTH - TAB_HEIGHT - TOOLBAR_HEIGHT;
                        }
                        else
                        {
                            _last_mouse_position = Cursor.Position;
                        }
                        break;
                    }
            }
            return new_location;
        }

        // Toolbar拖曳移動處理
        private void ControlMove(Point displacement)
        {
            Point new_location = new Point(this.Location.X, this.Location.Y);

            new_location.X += displacement.X;
            new_location.Y += displacement.Y;

            this.Location = LimitControlMove(new_location);
        }

        // 檢查滑鼠位置並回傳Toolbar應放置位置方向
        private Direction CheckCursorPosition()
        {
            if (Cursor.Position.X >= _resolution.Width * 0.2 && Cursor.Position.X <= _resolution.Width * 0.8
                && Cursor.Position.Y <= _resolution.Height * 0.2)
            {
                return Direction.Top;
            }
            else if (Cursor.Position.X >= _resolution.Width * 0.8
                && Cursor.Position.Y >= _resolution.Height * 0.2 && Cursor.Position.Y <= _resolution.Height * 0.8)
            {
                return Direction.Right;
            }
            else if (Cursor.Position.X >= _resolution.Width * 0.2 && Cursor.Position.X <= _resolution.Width * 0.8
                && Cursor.Position.Y >= _resolution.Height * 0.8)
            {
                return Direction.Bottom;
            }
            else if (Cursor.Position.X <= _resolution.Width * 0.2
                && Cursor.Position.Y >= _resolution.Height * 0.2 && Cursor.Position.Y <= _resolution.Height * 0.8)
            {
                return Direction.Left;
            }
            return _curr_direction;
        }

        // Toolbar切換位置
        private void SwitchDirection(Direction new_direction)
        {
            if (_curr_direction == new_direction) return;
            _curr_direction = new_direction;
            switch (_curr_direction)
            {
                case Direction.Top:
                    {
                        tableLayoutPanel_top_function.Controls.Add(button_exit, 6, 0);
                        tableLayoutPanel_top_function.Controls.Add(button_func3, 4, 0);
                        tableLayoutPanel_top_function.Controls.Add(button_func2, 2, 0);
                        tableLayoutPanel_top_function.Controls.Add(button_func1, 0, 0);
                        break;
                    }
                case Direction.Right:
                    {
                        tableLayoutPanel_right_function.Controls.Add(button_exit, 0, 6);
                        tableLayoutPanel_right_function.Controls.Add(button_func3, 0, 4);
                        tableLayoutPanel_right_function.Controls.Add(button_func2, 0, 2);
                        tableLayoutPanel_right_function.Controls.Add(button_func1, 0, 0);
                        break;
                    }
                case Direction.Bottom:
                    {
                        tableLayoutPanel_bottom_function.Controls.Add(button_exit, 6, 0);
                        tableLayoutPanel_bottom_function.Controls.Add(button_func3, 4, 0);
                        tableLayoutPanel_bottom_function.Controls.Add(button_func2, 2, 0);
                        tableLayoutPanel_bottom_function.Controls.Add(button_func1, 0, 0);
                        break;
                    }
                case Direction.Left:
                    {
                        tableLayoutPanel_left_function.Controls.Add(button_exit, 0, 6);
                        tableLayoutPanel_left_function.Controls.Add(button_func3, 0, 4);
                        tableLayoutPanel_left_function.Controls.Add(button_func2, 0, 2);
                        tableLayoutPanel_left_function.Controls.Add(button_func1, 0, 0);
                        break;
                    }
            }
            ChangeRegion(_curr_direction, _is_hide);
            ChangeLocation(_curr_direction, _is_hide);
        }


        #region 私有成員參數、函式

        Rectangle _resolution = Screen.PrimaryScreen.Bounds;

        private bool _mouse_down = false;
        private bool _mouse_moving = false;
        private Point _last_mouse_position;

        /// <summary>
        /// 定義Toolbar位置方向
        /// </summary>
        private enum Direction
        {
            Top = 0,
            Right = 1,
            Bottom = 2,
            Left = 3
        }

        // Toolbar當前位置
        private Direction _curr_direction = Direction.Top;
        private bool _is_hide = true;

        // 定義相關控制項屬性數值
        private const int FUNC_WIDTH = 60;
        private const int FUNC_HEIGHT = 60;

        private const int TAB_WIDTH = 30;
        private const int TAB_HEIGHT = 20;
        private const int TAB_DISTANCE = 20;

        private const int MARGIN_SIZE = 5;

        private const int TOOLBAR_WIDTH = FUNC_WIDTH * 4 + MARGIN_SIZE * 5;
        private const int TOOLBAR_HEIGHT = FUNC_HEIGHT + MARGIN_SIZE * 2;

        private const int FORM_WIDTH = TOOLBAR_HEIGHT * 2 + TAB_HEIGHT * 2 + TOOLBAR_WIDTH;
        private const int FORM_HEIGHT = FORM_WIDTH;



        // 定義Toolbar展開時不同位置的視窗遮罩點座標
        private static readonly Point[][] _region_list = new Point[][]
            {
                new Point[]
                {
                    new Point(TOOLBAR_HEIGHT+TAB_HEIGHT, 0),
                    new Point(TOOLBAR_HEIGHT+TAB_HEIGHT+TOOLBAR_WIDTH, 0),
                    new Point(TOOLBAR_HEIGHT+TAB_HEIGHT+TOOLBAR_WIDTH, TOOLBAR_HEIGHT),
                    new Point(TOOLBAR_HEIGHT+TAB_HEIGHT+TOOLBAR_WIDTH-TAB_DISTANCE, TOOLBAR_HEIGHT),
                    new Point(TOOLBAR_HEIGHT+TAB_HEIGHT+TOOLBAR_WIDTH-TAB_DISTANCE, TOOLBAR_HEIGHT+TAB_HEIGHT),
                    new Point(TOOLBAR_HEIGHT+TAB_HEIGHT+TOOLBAR_WIDTH-TAB_DISTANCE-TAB_WIDTH, TOOLBAR_HEIGHT+TAB_HEIGHT),
                    new Point(TOOLBAR_HEIGHT+TAB_HEIGHT+TOOLBAR_WIDTH-TAB_DISTANCE-TAB_WIDTH, TOOLBAR_HEIGHT),
                    new Point(TOOLBAR_HEIGHT+TAB_HEIGHT, TOOLBAR_HEIGHT)
                },
                new Point[]
                {
                    new Point(FORM_WIDTH-TOOLBAR_HEIGHT, TOOLBAR_HEIGHT+TAB_HEIGHT),
                    new Point(FORM_WIDTH, TOOLBAR_HEIGHT+TAB_HEIGHT),
                    new Point(FORM_WIDTH, TOOLBAR_HEIGHT+TAB_HEIGHT+TOOLBAR_WIDTH),
                    new Point(FORM_WIDTH-TOOLBAR_HEIGHT, TOOLBAR_HEIGHT+TAB_HEIGHT+TOOLBAR_WIDTH),
                    new Point(FORM_WIDTH-TOOLBAR_HEIGHT, TOOLBAR_HEIGHT+TAB_HEIGHT+TAB_DISTANCE+TAB_WIDTH),
                    new Point(FORM_WIDTH-TOOLBAR_HEIGHT-TAB_HEIGHT, TOOLBAR_HEIGHT+TAB_HEIGHT+TAB_DISTANCE+TAB_WIDTH),
                    new Point(FORM_WIDTH-TOOLBAR_HEIGHT-TAB_HEIGHT, TOOLBAR_HEIGHT+TAB_HEIGHT+TAB_DISTANCE),
                    new Point(FORM_WIDTH-TOOLBAR_HEIGHT, TOOLBAR_HEIGHT+TAB_HEIGHT+TAB_DISTANCE)
                },
                new Point[]
                {
                    new Point(FORM_WIDTH-TOOLBAR_HEIGHT-TAB_HEIGHT, FORM_HEIGHT),
                    new Point(FORM_WIDTH-TOOLBAR_HEIGHT-TAB_HEIGHT-TOOLBAR_WIDTH, FORM_HEIGHT),
                    new Point(FORM_WIDTH-TOOLBAR_HEIGHT-TAB_HEIGHT-TOOLBAR_WIDTH, FORM_HEIGHT-TOOLBAR_HEIGHT),
                    new Point(FORM_WIDTH-TOOLBAR_HEIGHT-TAB_HEIGHT-TAB_DISTANCE-TAB_WIDTH, FORM_HEIGHT-TOOLBAR_HEIGHT),
                    new Point(FORM_WIDTH-TOOLBAR_HEIGHT-TAB_HEIGHT-TAB_DISTANCE-TAB_WIDTH, FORM_HEIGHT-TOOLBAR_HEIGHT-TAB_HEIGHT),
                    new Point(FORM_WIDTH-TOOLBAR_HEIGHT-TAB_HEIGHT-TAB_DISTANCE, FORM_HEIGHT-TOOLBAR_HEIGHT-TAB_HEIGHT),
                    new Point(FORM_WIDTH-TOOLBAR_HEIGHT-TAB_HEIGHT-TAB_DISTANCE, FORM_HEIGHT-TOOLBAR_HEIGHT),
                    new Point(FORM_WIDTH-TOOLBAR_HEIGHT-TAB_HEIGHT, FORM_HEIGHT-TOOLBAR_HEIGHT)
                },
                new Point[]
                {
                    new Point(0, TOOLBAR_HEIGHT+TAB_HEIGHT),
                    new Point(TOOLBAR_HEIGHT, TOOLBAR_HEIGHT+TAB_HEIGHT),
                    new Point(TOOLBAR_HEIGHT, TOOLBAR_HEIGHT+TAB_HEIGHT+TAB_DISTANCE),
                    new Point(TOOLBAR_HEIGHT+TAB_HEIGHT, TOOLBAR_HEIGHT+TAB_HEIGHT+TAB_DISTANCE),
                    new Point(TOOLBAR_HEIGHT+TAB_HEIGHT, TOOLBAR_HEIGHT+TAB_HEIGHT+TAB_DISTANCE+TAB_WIDTH),
                    new Point(TOOLBAR_HEIGHT, TOOLBAR_HEIGHT+TAB_HEIGHT+TAB_DISTANCE+TAB_WIDTH),
                    new Point(TOOLBAR_HEIGHT, TOOLBAR_HEIGHT+TAB_HEIGHT+TOOLBAR_WIDTH),
                    new Point(0, TOOLBAR_HEIGHT+TAB_HEIGHT+TOOLBAR_WIDTH)
                }
            };

        // 定義Toolbar收起時不同位置的視窗遮罩點座標
        private static readonly Point[][] _hide_region_list = new Point[][]
            {
                new Point[]
                {
                    new Point(TOOLBAR_HEIGHT+TAB_HEIGHT+TOOLBAR_WIDTH-TAB_DISTANCE, TOOLBAR_HEIGHT),
                    new Point(TOOLBAR_HEIGHT+TAB_HEIGHT+TOOLBAR_WIDTH-TAB_DISTANCE, TOOLBAR_HEIGHT+TAB_HEIGHT),
                    new Point(TOOLBAR_HEIGHT+TAB_HEIGHT+TOOLBAR_WIDTH-TAB_DISTANCE-TAB_WIDTH, TOOLBAR_HEIGHT+TAB_HEIGHT),
                    new Point(TOOLBAR_HEIGHT+TAB_HEIGHT+TOOLBAR_WIDTH-TAB_DISTANCE-TAB_WIDTH, TOOLBAR_HEIGHT),
                },
                new Point[]
                {
                    new Point(FORM_WIDTH-TOOLBAR_HEIGHT, TOOLBAR_HEIGHT+TAB_HEIGHT+TAB_DISTANCE+TAB_WIDTH),
                    new Point(FORM_WIDTH-TOOLBAR_HEIGHT-TAB_HEIGHT, TOOLBAR_HEIGHT+TAB_HEIGHT+TAB_DISTANCE+TAB_WIDTH),
                    new Point(FORM_WIDTH-TOOLBAR_HEIGHT-TAB_HEIGHT, TOOLBAR_HEIGHT+TAB_HEIGHT+TAB_DISTANCE),
                    new Point(FORM_WIDTH-TOOLBAR_HEIGHT, TOOLBAR_HEIGHT+TAB_HEIGHT+TAB_DISTANCE),
                },
                new Point[]
                {
                    new Point(FORM_WIDTH-TOOLBAR_HEIGHT-TAB_HEIGHT-TAB_DISTANCE-TAB_WIDTH, FORM_HEIGHT-TOOLBAR_HEIGHT),
                    new Point(FORM_WIDTH-TOOLBAR_HEIGHT-TAB_HEIGHT-TAB_DISTANCE-TAB_WIDTH, FORM_HEIGHT-TOOLBAR_HEIGHT-TAB_HEIGHT),
                    new Point(FORM_WIDTH-TOOLBAR_HEIGHT-TAB_HEIGHT-TAB_DISTANCE, FORM_HEIGHT-TOOLBAR_HEIGHT-TAB_HEIGHT),
                    new Point(FORM_WIDTH-TOOLBAR_HEIGHT-TAB_HEIGHT-TAB_DISTANCE, FORM_HEIGHT-TOOLBAR_HEIGHT)
                },
                new Point[]
                {
                    new Point(TOOLBAR_HEIGHT, TOOLBAR_HEIGHT+TAB_HEIGHT+TAB_DISTANCE),
                    new Point(TOOLBAR_HEIGHT+TAB_HEIGHT, TOOLBAR_HEIGHT+TAB_HEIGHT+TAB_DISTANCE),
                    new Point(TOOLBAR_HEIGHT+TAB_HEIGHT, TOOLBAR_HEIGHT+TAB_HEIGHT+TAB_DISTANCE+TAB_WIDTH),
                    new Point(TOOLBAR_HEIGHT, TOOLBAR_HEIGHT+TAB_HEIGHT+TAB_DISTANCE+TAB_WIDTH)
                }
            };

        #endregion


        private void button_exit_Click(object sender, System.EventArgs e)
        {
            this.Close();
        }

        private void button_tab_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                _last_mouse_position = Cursor.Position;
                _mouse_down = true;
            }
        }

        private void button_tab_MouseMove(object sender, MouseEventArgs e)
        {
            if (_mouse_down)
            {
                _mouse_moving = true;
                ControlMove(GetMouseMoveDisplacement());
                SwitchDirection(CheckCursorPosition());
            }
        }

        private void button_tab_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                _mouse_down = false;
                _mouse_moving = false;
            }
        }

        private void button_tab_Click(object sender, System.EventArgs e)
        {
            if (!_mouse_moving)
                SwitchHideState();
        }
    }
}
